require('dotenv').config()
const express = require('express')
const mongoose = require('mongoose')

// Connect to mongo database and start app
mongoose
  .connect(process.env.MONGO_URI, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => {
    const app = express()
    app.use('/', require('./api'))
    app.listen(3000, () => console.log('Listening on port 3000'))
  })
