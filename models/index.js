const mongoose = require('mongoose')
const generator = require('oni-wp-mongoose-model-generator')

generator.generate({
  acfExport: require('./acf-export'),
  postTypesExport: require('./cptui_post_types'),
  taxonomiesExport: require('./cptui_taxonomies'),
  mongoose
})
